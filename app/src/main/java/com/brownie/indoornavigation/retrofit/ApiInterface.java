package com.brownie.indoornavigation.retrofit;

import com.brownie.indoornavigation.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("getAllC2Cs")
    Call<List<User>> getAllC2Cs();
}
