package com.brownie.indoornavigation;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mapsindoors.mapssdk.MPLocation;
import com.mapsindoors.mapssdk.MapControl;
import com.mapsindoors.mapssdk.MapsIndoors;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private SupportMapFragment mapFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        MapsIndoors.initialize(
                getApplicationContext(),
                "57e4e4992e74800ef8b69718",
                "AIzaSyCUIdqpy6fJtEZ-KRGFkbtuXA_8T1EyxWY"
        );
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
*/
        // Create a new MapControl instance
        MapControl myMapControl = new MapControl( this, mapFragment, mMap );

        DemoPositionProvider demoPositionProvider = new DemoPositionProvider();

        MapsIndoors.setPositionProvider( demoPositionProvider );

        myMapControl.showUserPosition( true );

        // Initialize MapControl to get the locations on the map, etc.
        myMapControl.init( errorCode -> {
            if( errorCode == null ) {
                runOnUiThread( () -> {
                    // Animate the camera to show the venue
                    mMap.animateCamera( CameraUpdateFactory.newLatLngZoom( new LatLng( 57.05813067, 9.95058065 ), 19f ) );
                });
            }
        });

        myMapControl.setOnMarkerInfoWindowClickListener( marker -> {
            startActivity(new Intent(MapsActivity.this, DirectionsActivity.class));
        });
        /*
        myMapControl.setOnMarkerInfoWindowCloseListener( marker -> {
            startActivity(new Intent(MapsActivity.this, DirectionsActivity.class));
        } );*/

    }


}
