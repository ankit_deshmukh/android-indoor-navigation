package com.brownie.indoornavigation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.brownie.indoornavigation.model.Directions;

import java.util.ArrayList;

public class DirectionsActivity extends AppCompatActivity {

    private static final String TAG = "DirectionActivity";

    private ArrayList<Directions> mDirections = new ArrayList<>();

    private RecyclerView directionsRecyclerView;

    private RecyclerViewAdapter directionRecycAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);

        initList();
    }

    private void initList()
    {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mDirections.add(new Directions("Go Back","1 metre","1 min","https://collabcowork.s3.ap-south-1.amazonaws.com/downward.jpeg"));

        mDirections.add(new Directions("Go Forward","2 metre","2 min","https://collabcowork.s3.ap-south-1.amazonaws.com/forward.jpeg"));

        mDirections.add(new Directions("Go Left","0.5 metre","30 seconds","https://collabcowork.s3.ap-south-1.amazonaws.com/left.jpeg"));

        mDirections.add(new Directions("Go Right","1 metre","2 min","https://collabcowork.s3.ap-south-1.amazonaws.com/right.jpeg"));

        setupRecyclerView();
    }

    private void setupRecyclerView()
    {
        Log.d(TAG, "initRecyclerView: init recyclerview");

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);

        directionsRecyclerView = findViewById(R.id.review_recycler_view);

        directionRecycAdapter = new RecyclerViewAdapter(this, mDirections);

        directionsRecyclerView.setAdapter(directionRecycAdapter);
    }
}
