package com.brownie.indoornavigation;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.brownie.indoornavigation.model.Directions;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.HomeViewHolder>{

    private static final String TAG = "RecyclerViewAdapater";

    // vars //
    private ArrayList<String> cafeNames = new ArrayList<>();
    private ArrayList<String> cafeImageUrls = new ArrayList<>();
    private ArrayList<String> itemsFiltered = new ArrayList<>();
    private Directions directionData;

    private List<Directions> dataset;
    private Context mContext;

    public RecyclerViewAdapter(Context mContext, List<Directions> directionDataset)
    {
        this.mContext = mContext;
        this.dataset = directionDataset;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: called.");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .asBitmap()
                .load(directionData.getImageUrl())
                .into(holder.dirImage);

        holder.distance.setText(directionData.getDistance());
        holder.direction.setText(directionData.getDirection());
        holder.time.setText(directionData.getTime());
        /*holder.row_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                directionData = dataset.get(position);
                FragmentManager fragmentManager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                Bundle c2cBundle = new Bundle();
                c2cBundle.putSerializable("C2C_DATA", directionData);
                fragmentManager.beginTransaction().replace(R.id.fragment_container, bottomSheetFragment).addToBackStack(null).commit();
                bottomSheetFragment.setArguments(c2cBundle);

                Log.d(TAG, "onClick: directionData= " + directionData);
                Log.d(TAG, "onClick: card row clicked! ");
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder
    {
        ConstraintLayout row_layout;
        ImageView dirImage;
        TextView distance, time, direction;


        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);

            row_layout = itemView.findViewById(R.id.card_row);
            dirImage = itemView.findViewById(R.id.dir_img);
            direction = itemView.findViewById(R.id.tv_direction);
            distance = itemView.findViewById(R.id.tv_distance);
            time = itemView.findViewById(R.id.tv_time);

        }
    }

}
