package com.brownie.indoornavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    private Button loginBtn;

    private EditText inputEmail, inputPass;

    private static final int SPLASH_SCREEN_DURATION = 2000;
    private static final int REQUEST_CODE = 1;
    private static final int PERMISSIONS_REQUEST_ACCESS_PHONE_UID = 2;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int MULTIPLE_PERMISSIONS = 10;

    // Boolean //
    private static boolean mLocationPermissionGranted, mPhoneStatePermissionGranted, isGpsOn, isInternetConnected, onChangeCalled, userLocationUpdated;

    // Strings //
    private String uid;
    private String permissions[];
    private List<String> listPermissionsNeeded;
    private String permissionsDenied;

    // Location Services //
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;
    private LatLng mDefaultLocation;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init()
    {
        inputEmail= findViewById(R.id.input_email);
        inputPass = findViewById(R.id.input_password);

        inputEmail.setHintTextColor(getResources().getColor(R.color.white));
        inputPass.setHintTextColor(getResources().getColor(R.color.white));


        loginBtn = findViewById(R.id.btn_logout);
        loginBtn.setOnClickListener(this);
    }

    private void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;

            getDeviceLocation();
            Log.d(TAG, "getLocationPermission: Location Permission Granted.");
        }
        else
        {
            Log.d(TAG, "getLocationPermission: Asking user for Location Permission");
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * The results of all the requested permissions are handled here
     */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Log.d(TAG, "onRequestPermissionsResult: called");

        mLocationPermissionGranted = false;
        mPhoneStatePermissionGranted = false;

        switch(requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0)
                {
                    permissionsDenied = "";

                    for (String per : permissions) {

                        for(int res : grantResults)
                        {
                            if (res == PackageManager.PERMISSION_DENIED)
                            {
                                permissionsDenied = per + "\n";
                                Log.d(TAG, "onRequestPermissionsResult: Permissions Denied : " + permissionsDenied);
                            }

                        }
                    }

                    if(permissionsDenied.isEmpty())
                    {
                        mLocationPermissionGranted = true;
                        mPhoneStatePermissionGranted = true;

                        listPermissionsNeeded.clear();
                        Log.d(TAG, "onRequestPermissionsResult: All permissions are granted.");
                    }
                }
            }
            break;

            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                /** If request is cancelled, the result arrays are empty. */

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mLocationPermissionGranted = true;
                    Log.d(TAG, "onRequestPermissionsResult: Location Permission Granted");

                    getDeviceLocation();

                }
                else
                {
                    Log.d(TAG, "onRequestPermissionsResult: location permission denied");
                }
            }
            break;

        }
    }

    private void getDeviceLocation() {
        try {

            if(mLocationPermissionGranted)
            {
                Log.d(TAG, "getDeviceLocation: called");

                mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful())
                        {
                            mLastKnownLocation = (Location) task.getResult();

                            if (mLastKnownLocation != null) {
                                Log.d(TAG, "onComplete: device location = " + mLastKnownLocation.getLatitude() + " " + mLastKnownLocation.getLongitude());

                            }
                        }
                        else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                        }
                    }
                });
            }

        }
        catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_logout)
        {
            startActivity(new Intent(this, MapsActivity.class));
        }
    }
}
