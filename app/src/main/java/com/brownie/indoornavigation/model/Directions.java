package com.brownie.indoornavigation.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Directions implements Serializable {

    @SerializedName("direction")
    private String direction;

    @SerializedName("distance")
    private String distance;

    @SerializedName("time")
    private String time;

    @SerializedName("imageUrl")
    private String imageUrl;

    public Directions() {
    }

    public Directions(String direction, String distance, String time, String imageUrl) {
        this.direction = direction;
        this.distance = distance;
        this.time = time;
        this.imageUrl = imageUrl;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Directions{" +
                "direction='" + direction + '\'' +
                ", distance='" + distance + '\'' +
                ", time='" + time + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
